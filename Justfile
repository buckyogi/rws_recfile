
# Display recipes
default:
  just -ul

# Display DESCRIPTION for card 'num' (0-77)
art num:
  (recsel -p ID,NAME -e "ID = '{{num}}'" -t Card rws.rec && recsel -p DESCRIPTION -e "ID = '{{num}}'" -t Description rws_description.rec) | bat

# Display info for card number 'num' (0-77)
card num:
  recsel -p NAME,UPRIGHT,REVERSE,SYMBOL,NOTES -e "ID = '{{num}}'" -t Card rws.rec | bat

# Display symbols for entire deck
imagery:
  recsel -p ID,NAME,SYMBOL -t Card rws.rec | bat

# Print entire deck
print:
  recsel -p ID,NAME -t Card rws.rec | bat

# Draw and display random card
random:
  (recsel -p ID,NAME -m 1 -t Card rws.rec; ./uprev.zsh) | bat

# Display suits information
suits:
  recsel -p NAME,ELEMENT,CLASS,FRENCH,REALM -t Suit rws.rec | bat

# Display cards with SYMBOL 'sym' (for list of symbols: % bat symbols.txt)
symbol sym:
  recsel -p NAME -e "SYMBOL = '{{sym}}'" -t Card rws.rec | grep 'NAME' | bat

