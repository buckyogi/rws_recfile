#!/usr/bin/zsh
set +x # tracing; -x to enable, +x to disable

uprev=$((${RANDOM}%3+1))
if [[ uprev -lt 3 ]]
then
    echo "Upright"
else
    echo "Reverse"
fi

exit 0

