# Changelog

As this is an informal project, so will this changelog be informal.

## 2025-02-10

### Added
- **uprev.zsh**: Shell script to generate Upright/Reverse card in "random" recipe in Justfile

### Changed
- **Justfile**: Modified "random" recipe to use *uprev.zsh*
- **README.md**: Modified *rws_description.rec* description; remove *uprev.c*; add *uprev.zsh*
- **LICENSE**: Updated copyright date

### Removed
- Removed **upright.c**

## 2024-03-09

### Changed
- **rws.rec**, **rws_description.rec**: Removed blank lines between descriptors so recinf works correctly
- **Justfile**: Changed name of MEANING field to UPRIGHT

## 2024-03-08

### Changed
- Modified definiton of ID field regexp and edited fields accordingly (rws.rec, rws_description.rec)
- Changed name of MEANING field to UPRIGHT

## 2024-02-22

### Removed

- Description comment in rws.rec header (moved to rws_description.rec)

## 2024-02-19

I have made a great many changes since the last commit, mostly stylistic, few substantive.

### Added
- Field: SKY
- Enumerated SKY types: Black Blue Cloudy Divided Maroon None Yellow 
- Enumerated SYMBOLS types: Indoors, Mist, Paloma
- Added above SYMBOLS to symbols.txt
- CHANGELOG.md

### Changed
- Changed record sets to Suit, Card, Description
- Moved DESCRIPTION fields to separate record set
- Set each sentence of each description on a separate line.
- Moved Description record set toe separate file (rws_description.rec)
- Edited Justfile to reflect changes in recfiles

