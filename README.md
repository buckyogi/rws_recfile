# What is this project?
---
**rws.rec** : My Smith-Waite-Rider Tarot deck recfile.  
**rws_description.rec** : Card descriptions recfile.  
**Justfile** : command runner I use to implement recutils.  
**symbols.txt** : list of symbols used in SYMBOLS fields. Quick reference for the "symbol" recipe in the Justfile.  
**uprev.zsh** : a zsh shell script that randomly prints "Upright" two-thirds of the time and "Reverse" one-third of the time. Adjust percentages as desired, or remove from recipe to always draw upright card. Used in the "random" recipe in the Justfile. Change the shebang to your shell, if needed.

The original recfile is from [dozens / tarot](https://git.tilde.town/dozens/tarot/). I have extensively modified it.

---

For more information on recfiles see: [GNU Recutils](https://www.gnu.org/software/recutils/)

---

For more information on Justfiles see:
[just Command Runner](https://github.com/casey/just/)

---

For more information on bat (cat replacement used in Justfile) see:
[Bat: A cat clone with wings](https://github.com/sharkdp/bat/)

---

For more information on Git see:
[Official Git Documentation](https://git-scm.com/)

---
